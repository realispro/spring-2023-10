package spring;

import org.springframework.stereotype.Component;
import spring.config.ExecutionTime;
import spring.config.TicketControl;

@Component
public class Travel {

    private final String name;
    private final Transportation transportation;
    private final Accomodation accomodation;

    public Travel(Transportation transportation, Accomodation accomodation, String name) {
        this.transportation = transportation;
        this.accomodation = accomodation;
        this.name = name;
        System.out.println("constructing travel with parametrized constructor...");
    }

/*    public Travel() {
        System.out.println("constructing travel using default constructor");
    }*/

    @TicketControl
    public void intro(){
        System.out.println("travel will start soon");
    }

    @ExecutionTime
    @TicketControl
    public void travel(Person p){
        System.out.println("started travel " + name + " for a person " + p);
        transportation.transport(p);
        accomodation.host(p);
        transportation.transport(p);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Travel{" +
                "name='" + name + '\'' +
                ", transportation=" + transportation +
                ", accomodation=" + accomodation +
                '}';
    }
}
