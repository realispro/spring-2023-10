package spring.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import spring.Person;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;

@Component
@Aspect
public class TravelAspect {

    @Pointcut("execution(public * spring.impl..*(..))")
    void allInImpl(){}

    @Before("allInImpl()")
    void logEnteringMethod(JoinPoint jp){
        System.out.println("[ENTERING] " + jp.toLongString() + " on " + jp.getTarget().getClass().getName());
    }

    @After("allInImpl() && execution(* spring..*(spring.Person)))")
    void logExitingMethod(JoinPoint jp){
        Person person = (Person) jp.getArgs()[0];
        System.out.println("[EXITING] " + jp.toLongString() + " on " + jp.getTarget().getClass().getName());
    }

    @Around("@annotation(spring.config.ExecutionTime)")
    Object logExecutionTime(ProceedingJoinPoint jp) throws Throwable {

        Instant start = Instant.now();
        Object result = jp.proceed(jp.getArgs());
        Instant end = Instant.now();
        System.out.println("[EXECUTION TIME] " + jp.toLongString() + " execution took " + Duration.between(start, end));

        return result;
    }

    @Before("@annotation(spring.config.TicketControl) && execution(* *(spring.Person))")
    void checkIfTicketIsValid(JoinPoint jp) {
        Person person = (Person) jp.getArgs()[0];
        if (person.getTicket().getValid().isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("ticket not valid");
        }
    }

}
