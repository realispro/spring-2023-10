package spring.config;

import org.springframework.context.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan("spring")
@PropertySource("classpath:/meals.properties")
@EnableAspectJAutoProxy
public class TravelConfig {

    @Bean("travelName")
    String travelName(){
        return "Business travel";
    }

    @Bean
    List<String> meals(){
        return new ArrayList<>(List.of(
                "hamburger",
                "coca-cola"
        ));
    }

}
