package spring.impl;


import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import spring.Accomodation;
import spring.Person;
import spring.config.ExecutionTime;

import java.util.List;


@Component("accomodation")
public class Hotel implements Accomodation {

    @Value("#{'${meal.gratis:tea}'.toUpperCase()}") // SpEL
    private String gratis;

    //@Autowired
    //@Qualifier("meals")
    //@Resource
    @Value("#{'${meal.menu}'.split(',')}")
    private List<String> meals;

    @PostConstruct
    private void addGratis(){
        meals.add(gratis);
    }

    public void setMeals(List<String> meals) {
        this.meals = meals;
    }

    @ExecutionTime
    @Override
    public void host(Person p) {
        System.out.println("person " + p + " is being hosted in hotel. meal: " + meals);
    }
}
