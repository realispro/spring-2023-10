package spring.impl;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import spring.Person;
import spring.Transportation;

@Component("plane")
@Primary
@Air
public class Plane implements Transportation {

    @Override
    public void transport(Person p) {
        System.out.println("Person " + p + " is being transported by plane");
    }

    @PostConstruct
    public void postConstruct(){
        System.out.println("plane: post construct");
    }

    @PreDestroy
    public void preDestroy(){
        System.out.println("plane: pre destroy");
    }

}
