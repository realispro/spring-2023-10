package vod.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import vod.web.dto.MovieDTO;
import vod.web.dto.RatingDTO;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Slf4j
public class VodClient {

    public static void main(String[] args) {
        log.info("Let's list movies!");

        String serviceBaseUrl = "http://localhost:8080/vod/webapi";
        RestTemplate restTemplate = new RestTemplate();


        // request for single movie

        int movieId = 404;
        try {
            ResponseEntity<MovieDTO> response = restTemplate.exchange(
                    serviceBaseUrl + "/movies/" + movieId,
                    HttpMethod.GET,
                    HttpEntity.EMPTY,
                    MovieDTO.class
            );

            log.info("movie map received: {}, response code: {}",
                    response.getBody(),
                    response.getStatusCode());
        }catch (HttpClientErrorException.NotFound e){
            log.error("movie not found", e);
        }

        // request for all movies

        ResponseEntity<MovieDTO[]> responseForMany = restTemplate.exchange(
                serviceBaseUrl + "/movies",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                MovieDTO[].class
        );

        log.info("movies received {}", Arrays.toString(responseForMany.getBody()));

        // rate movie
        RatingDTO rating = new RatingDTO();
        rating.setMovieId(movieId);
        rating.setRate(5.1F);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept-Language", "pl");
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<RatingDTO> httpEntity = new HttpEntity<>(rating, headers);

        try {
            ResponseEntity<RatingDTO> responseRating = restTemplate.exchange(
                    serviceBaseUrl + "/ratings",
                    HttpMethod.POST,
                    httpEntity,
                    RatingDTO.class
            );

            log.info("rating created: {}, location: {}",
                    responseRating.getBody(),
                    responseRating.getHeaders().get("Location"));
        }catch (HttpClientErrorException.BadRequest e){
            log.info("validation errors {}", Arrays.toString(e.getResponseBodyAs(String[].class)));
        }


    }
}
