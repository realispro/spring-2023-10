package vod.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vod.model.Movie;
import vod.model.Rating;

import java.util.List;
import java.util.Optional;

public interface RatingDao extends JpaRepository<Rating, Integer> {

    //Rating save(Rating rating);

    List<Rating> findAllByMovie(Movie movie);

    List<Rating> findAllByRateGreaterThanEqual(float rate);

    //Optional<Rating> findById(int ratingId);
}
