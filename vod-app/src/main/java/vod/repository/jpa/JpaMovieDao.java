package vod.repository.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vod.model.Cinema;
import vod.model.Director;
import vod.model.Movie;
import vod.repository.MovieDao;

import java.util.List;
import java.util.Optional;

@Repository
@Primary
public abstract class JpaMovieDao implements MovieDao {

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public List<Movie> findAll() {
        return entityManager.createQuery("select m from Movie m", Movie.class).getResultList();

    }

    @Override
    public Optional<Movie> findById(Integer id) {
        return Optional.ofNullable(entityManager.find(Movie.class, id));
    }

    @Override
    public List<Movie> findByDirector(Director d) {
        return entityManager.createQuery("select m from Movie m where m.director=:director", Movie.class)
                .setParameter("director", d)
                .getResultList();

    }

    @Override
    public List<Movie> findByCinema(Cinema c) {
        return entityManager.createQuery("select m from Movie m inner join m.cinemas cinema where cinema=:cinema", Movie.class)
                .setParameter("cinema",c)
                .getResultList();

    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public Movie save(Movie m) {
        entityManager.persist(m);
        return m;
    }
}