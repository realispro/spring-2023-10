package vod.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;
import vod.web.dto.CinemaDTO;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
public class CinemaController {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping("/cinemas") // /cinemas?foo=bar&abc=xyz
    List<CinemaDTO> getCinemas(
            @RequestParam(value = "foo", required = false) String foo,
            @RequestHeader(value = "User-Agent", required = false) String userAgent,
            @RequestHeader Map<String,String> headers,
            @CookieValue(value = "custom-cookie", required = false) String customCookie
            ) {
        log.info("about to retrieve cinemas");
        log.info("custom cookie: {}", customCookie);
        log.info("foo request param: {}", foo);
        log.info("userAgent header: {}", userAgent);
        headers.entrySet()
                .forEach(entry->log.info("header name: {}, value: {}", entry.getKey(), entry.getValue()));

        List<Cinema> cinemas = cinemaService.getAllCinemas();
        return cinemas.stream()
                .map(cinema -> toDto(cinema))
                .toList();
    }

    @GetMapping("/cinemas/{cinemaId}")
    ResponseEntity<?> getCinema(@PathVariable("cinemaId") int cinemaId) {
        log.info("about to retrieve cinema {}", cinemaId);
        Cinema cinema = cinemaService.getCinemaById(cinemaId);

        return ResponseEntity.ofNullable(
                Optional.ofNullable(cinema)
                        .map(c -> toDto(c))
                        .orElse(null)
        );
    }

    @GetMapping("/movies/{movieId}/cinemas")
    ResponseEntity<List<CinemaDTO>> getCinemasByMovie(@PathVariable("movieId") int movieId){
        log.info("about to retrieve cinemas showing movie {}", movieId);

        Movie movie = movieService.getMovieById(movieId);
        if(movie!=null) {
            List<Cinema> cinemas = cinemaService.getCinemasByMovie(movie);
            return ResponseEntity.ok(cinemas.stream().map(this::toDto).toList());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    private CinemaDTO toDto(Cinema cinema) {
        CinemaDTO dto = new CinemaDTO();
        dto.setId(cinema.getId());
        dto.setName(cinema.getName());
        dto.setLogo(cinema.getLogo());
        return dto;
    }
}
