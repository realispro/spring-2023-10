package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.model.Cinema;
import vod.model.Director;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;
import vod.web.dto.MovieDTO;

import java.net.URI;
import java.util.*;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
public class MovieController {

    private final MovieService movieService;
    private final CinemaService cinemaService;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;
    private final MovieDTOValidator movieDTOValidator;

    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.addValidators(movieDTOValidator);
    }

    @GetMapping("/movies")
    List<MovieDTO> getMovies() {
        log.info("about to retrieve movies");
        List<Movie> movies = movieService.getAllMovies();
        return movies.stream().map(this::toDto).toList();
    }

    @GetMapping("movies/{movieId}")
    ResponseEntity<MovieDTO> getMovie(@PathVariable("movieId") int movieId) {
        log.info("about to retrieve movie {}", movieId);

        Movie movie = movieService.getMovieById(movieId);

        return ResponseEntity.ofNullable(Optional.ofNullable(movie).map(c -> toDto(c)).orElse(null));
    }

    @GetMapping("/cinemas/{cinemaId}/movies")
    ResponseEntity<List<MovieDTO>> getMoviesByCinema(@PathVariable("cinemaId") int cinemaId){
        log.info("about to retrieve movies shown in cinema {}", cinemaId);

        Cinema cinema = cinemaService.getCinemaById(cinemaId);

        if(cinema!=null) {
            List<Movie> movies = cinemaService.getMoviesInCinema(cinema);
            return ResponseEntity.ok(movies.stream().map(this::toDto).toList());
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(
            @Validated @RequestBody MovieDTO movieDTO,
            Errors errors,
            HttpServletRequest request){
        log.info("about to add movie {}", movieDTO);

        if(errors.hasErrors()){
            Locale locale = localeResolver.resolveLocale(request);
            // messages_en_US
            // messages_en
            // messages

            List<String> messages = errors.getAllErrors().stream()
                    .map(oe-> messageSource.getMessage(oe.getCode(), oe.getArguments(), locale))
                            //String.format("error code: %s, args: %s", oe.getCode(), Arrays.toString(oe.getArguments())))
                    .toList();
            return ResponseEntity.badRequest().body(messages);
        }

        Movie movie = movieService.addMovie(toData(movieDTO));
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(Map.of("id", movie.getId()))
                .toUri();

        return ResponseEntity
                .created(uri)
                .body(toDto(movie));
    }

    private MovieDTO toDto(Movie movie) {
        MovieDTO movieDTO = new MovieDTO();
        movieDTO.setId(movie.getId());
        movieDTO.setTitle(movie.getTitle());
        movieDTO.setPoster(movie.getPoster());
        movieDTO.setDirectorId(movie.getDirector().getId());

        return movieDTO;
    }

    private Movie toData(MovieDTO dto){
        Movie movie = new Movie();
        movie.setId(dto.getId());
        movie.setTitle(dto.getTitle());
        movie.setPoster(dto.getPoster());
        Director director = movieService.getDirectorById(dto.getDirectorId());
            //new Director();
            //director.setId(dto.getDirectorId());
        movie.setDirector(director);
        return movie;
    }
}