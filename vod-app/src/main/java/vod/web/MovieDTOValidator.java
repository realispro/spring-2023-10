package vod.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import vod.model.Director;
import vod.service.MovieService;
import vod.web.dto.MovieDTO;

@Component
@RequiredArgsConstructor
public class MovieDTOValidator implements Validator {

    private final MovieService movieService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(MovieDTO.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MovieDTO dto = (MovieDTO) target;

        Director director = movieService.getDirectorById(dto.getDirectorId());
        if(director==null){
            errors.rejectValue(
                    "directorId",
                    "errors.director.missing",
                    new Object[]{dto.getDirectorId()},
                    "Director is missing"
                    );
        }
    }
}
