package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.model.Movie;
import vod.model.Rating;
import vod.service.MovieService;
import vod.web.dto.RatingDTO;

import java.net.URI;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
public class RatingController {

    private final MovieService movieService;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;
    private final RatingDTOValidator ratingDTOValidator;

    @InitBinder
    void initBinder(WebDataBinder binder) {
        binder.addValidators(ratingDTOValidator);
    }

    @GetMapping("/ratings")
    ResponseEntity<List<RatingDTO>> getRatings(
            @RequestParam(value = "movieId") int movieId
    ) {
        log.info("about to retrieve ratings");
        log.info("movieId request param {}", movieId);

        Movie movie = movieService.getMovieById(movieId);

        if(movie != null) {
            List<Rating> ratings = movieService.getRatingsByMovie(movie);
                    //ratingService.getRatingsByMovie(movie);

            List<RatingDTO> result = ratings.stream()
                    .map(this::toRatingDTO)
                    .toList();

            return ResponseEntity.ok(result);
        }

        return ResponseEntity.notFound().build();
    }

    @GetMapping("/ratings/{ratingId}")
    ResponseEntity<RatingDTO> getRating(@PathVariable int ratingId){

        Rating rating = movieService.getRatingById(ratingId);

        return ResponseEntity.ofNullable(
                Optional.ofNullable(rating)
                        .map(r->toRatingDTO(r))
                        .orElse(null));
    }

    @PostMapping("/ratings")
    ResponseEntity<?> addRating(@Validated @RequestBody RatingDTO ratingDTO,
                               Errors errors,
                               HttpServletRequest request) {
        log.info("adding rating {}", ratingDTO);

        if(errors.hasErrors()) {
            Locale locale = localeResolver.resolveLocale(request);
//            Locale locale = Locale.ITALIAN;
//            Locale locale = Locale.getDefault();
            //messages_en_US
            //messages_en
            //messages
            List<String> result = errors.getAllErrors().stream()
                    .map(oe ->
                            messageSource.getMessage(oe.getCode(), oe.getArguments(), locale))
//                            String.format("error code: %s, args: %s", oe.getCode(), Arrays.toString(oe.getArguments())))
                    .toList();
            return ResponseEntity.badRequest().body(result);
        }

        Rating rating = movieService.rateMovie(
                movieService.getMovieById(ratingDTO.getMovieId()),
                ratingDTO.getRate()
                );
                //ratingService.addRating(toData(ratingDTO));

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(Map.of("id", rating.getId()))
                .toUri();

        return ResponseEntity
                .created(uri)
//                .status(HttpStatus.CREATED)
                .body(toRatingDTO(rating));
    }

    private RatingDTO toRatingDTO(Rating rating) {
        RatingDTO ratingDTO = new RatingDTO();
        ratingDTO.setId(rating.getId());
        ratingDTO.setRate(rating.getRate());
        ratingDTO.setMovieId(rating.getMovie().getId());

        return ratingDTO;
    }

}
