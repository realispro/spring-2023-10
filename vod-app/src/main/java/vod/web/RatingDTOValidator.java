package vod.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import vod.model.Movie;
import vod.service.MovieService;
import vod.web.dto.RatingDTO;

@Component
@RequiredArgsConstructor
public class RatingDTOValidator implements Validator {

    private final MovieService movieService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(RatingDTO.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RatingDTO dto = (RatingDTO) target;

        Movie movie = movieService.getMovieById(dto.getMovieId());

        if(movie == null) {
            errors.rejectValue("movieId",
                    "errors.rating.movie.missing",
                    new Object[]{dto.getMovieId()},
                    "Invalid movie"
            );
        }
    }
}
