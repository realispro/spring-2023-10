package vod.web.dto;

import lombok.Data;

@Data
public class CinemaDTO {

    private int id;
    private String name;
    private String logo;
}
