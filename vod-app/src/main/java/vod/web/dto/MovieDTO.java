package vod.web.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class MovieDTO {
    private int id;
    @NotNull
    @Size(min = 2, max=100)
    private String title;
    @NotNull
    @Pattern(regexp = "(www|http:|https:)+[^\\s]+[\\w]")
    private String poster;
    private int directorId;
}
