package vod.web.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Data;

@Data
public class RatingDTO {

    private int id;
    private int movieId;
    @Min(0)
    @Max(5)
    private float rate;
}
