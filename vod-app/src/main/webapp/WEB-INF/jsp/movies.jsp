<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="./header.jsp" flush="true"/>

<table>
    <tr>
        <th>Poster</th>
        <th>Title</th>
        <th>Rating</th>
        <th>Director</th>
        <th>Cinemas</th>
        <c:if test="${ticket}">
            <th>Ticket</th>
        </c:if>
    </tr>
    <c:forEach var="m" items="${movies}">
        <tr>
            <td>
                <img src="${m.poster}"/>
            </td>
            <td>
                    ${m.title}
            </td>
            <td>
                <a href="./movies?directorId=${m.director.id}">${m.director.firstName} ${m.director.lastName}</a>
            </td>
            <td class="rating">

            </td>
            <td class="search-container">
                <a class="search" href="./cinemas?movieId=${m.id}"></a>
            </td>
            <c:if test="${ticket}">
                <td class="search-container">
                    <a href="./ticket?movieId=${m.id}&cinemaId=${cinemaId}">
                        buy
                    </a>
                </td>
            </c:if>
        </tr>
    </c:forEach>
</table>

<jsp:include page="./footer.jsp" flush="true"/>
