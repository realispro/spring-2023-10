package vod.test;

import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

public class MovieControllerTest {


    @Test
    void testExistingMovie(){
        RestAssured
                .given()
                .accept("application/json")
                .get("http://localhost:8080/vod/webapi/movies/4")
                .then()
                .statusCode(200)
                .assertThat()
                .body("id", Matchers.equalTo(4));

    }
}
