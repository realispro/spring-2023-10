package vod.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VodBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(VodBootApplication.class, args);
    }

}
