package vod.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vod.boot.model.Cinema;
import vod.boot.model.Movie;

import java.util.List;

public interface CinemaRepository extends JpaRepository<Cinema, Integer> {

    @Query("select c from Cinema c inner join c.movies movie where movie=:movie")
    List<Cinema> findByMovie(@Param("movie") Movie m);

}
