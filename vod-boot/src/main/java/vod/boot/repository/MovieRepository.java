package vod.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vod.boot.model.Movie;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
}
