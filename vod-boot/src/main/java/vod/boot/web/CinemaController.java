package vod.boot.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vod.boot.model.Cinema;
import vod.boot.repository.CinemaRepository;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
public class CinemaController {

    private final CinemaRepository cinemaRepository;

    @GetMapping("/cinemas")
    List<Cinema> getCinemas(){
        return cinemaRepository.findAll();
    }

    @GetMapping("/cinemas/{cinemaId}")
    Cinema getCinema(@PathVariable int cinemaId){
        return cinemaRepository.findById(cinemaId).orElse(null);
    }

}
